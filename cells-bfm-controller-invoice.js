class CellsBfmControllerInvoice extends extends Polymer.mixinBehaviors([ CellsBehaviors.i18nBehavior ], Polymer.Element) {

  static get is() {
    return 'cells-bfm-controller-invoice';
  }

  static get properties() {
    return {
      transactionPermision: Boolean
    };
  }

  _hasPerission(){
    this.dispatch(new CustomEvent('request-credentials'))
  }
}

customElements.define(CellsBfmControllerInvoice.is, CellsBfmControllerInvoice);
